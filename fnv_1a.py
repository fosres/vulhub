def fnv_1a(s,size):
	
	hash = 0xcbf29ce484222325

	i = 0

	while i < size:
		
		hash ^= s[i]

		hash *= 0x100000001b3

		i += 1

	return ( hash & 0xffffffffffffffff )


def fnv_hashattack():
	
	number = 0

	c = 0

	s = []

	while c < 4:

		old = number

		while number > 0:

			s.append(number & 0xff)

			number = number >> 8

		sl = bytearray(s)

		slen = len(sl)

		if fnv_1a(sl,slen) == 0xcbf29ce484222325:
			
			c += 1

			print(sl)

		s.clear()

		number = old

		number += 1

