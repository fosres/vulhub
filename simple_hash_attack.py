def simple_hash(s,size):
	
	hash = 0
	
	i = 0

	while i < size:
	
		hash += s[i]

		i += 1

	return hash % 30011
		

def simple_hashattack():
	
	number = 0
	
	old = 0

	c = 0

	s = []

	while c < 4 and number < 0xffffffffffffffffffffffffffffffff:

		old = number

		while number > 0:

			s.append(number & 0xff)

			number = number >> 8

		sl = bytearray(s)

		slen = len(sl)

		if simple_hash(sl,slen) == 0x0:
			
			c += 1

			print(sl)

		s.clear()

		number = old

		number += 1

simple_hashattack()
