/*
The work: "Denial of Service via Algorithmic Complexity Attacks"

explains an incredibly efficient algorithm to cause hash flooding

to take place rapidly.

This work was referenced by Daniel J. Bernstein and Jeanne-Phillip Aumasson

in their work

If you know the number of buckets and the algorithm. All one must

do is find enough inputs that make any of the buckets long enough 

to force a DoS attack.

Now go figure out how to apply this to Golang Maps: 

https://github.com/golang/go/blob/master/src/runtime/map.go

https://dave.cheney.net/2018/05/29/how-the-go-runtime-implements-maps-efficiently-without-generics
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

typedef unsigned long long int uint64;

typedef struct nlist
{
	struct nlist * l;
	
	uint64 val;


} nlist;

uint64 fnv_1a(unsigned char * s, uint64 size, uint64 n)
{
	uint64 hash = 0xcbf29ce484222325;

	uint64 i = 0;

	while 	( 
			(i < size)
		)
	{

		hash ^= s[i];

		hash *= 0x100000001b3;

		i++;

	}

	return hash % n;
}

unsigned char bucket_smash(unsigned table[], uint64 * i,uint64 * index,uint64 chain_len)
{
	if ( ++table[( (*index) = fnv_1a(i,8,sizeof(uint64)) )] >= chain_len )
	{	
		return 1;
	}

	*index = 0;

	return 0;	
}

int main(void)
{

	uint64 i = 0, f = 0, index = 0, target = 0;
	
	unsigned char c = 0;
	
	unsigned table[100019];

	memset(table,0x0,100019*sizeof(unsigned));

	while	( 
			( i < 0xffffffffffffffff )

			&&

			(
				( c = bucket_smash(table,&i,&index,0xffffff) )  == 0 
			)

		)
	{
			i++;
	}

	printf("c: %u\n\nindex: %llu\n\n",c,index);

	target = index;

	index = 0;

	memset(table,0x0,100019*sizeof(unsigned));

	i = 0;
	
	while	( 
			( i < 0xffffffffffffffff )

			&&

			(
				f < 0xffffff
			)

		)
	{
		
			bucket_smash(table,&i,&index,0xffffff);		

			if ( index == target )
			{
				printf("%llu\n",i);	

				f++;
			}
 
			i++;
	
	}
	
	return 0;

}
